﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	public BeeMove beePrefab;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;
    private float STimer;
    public float timeMin = 1.0f;
    public float timeMax = 3.0f;



    public void DestroyBees (Vector2 centre, float radius)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }

    }




    // Use this for initialization
    void Start()
    {
        StartCoroutine(AutoSpam());

    }


    IEnumerator AutoSpam()
    {
        while (true)
        {
            for (int i = 0; i < nBees; i++)
            { 
            STimer = Random.Range(timeMin, timeMax);
            yield return new WaitForSeconds(STimer);
            BeeMove bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
            bee.gameObject.name = "Bee " + i;
            float x = xMin + Random.value * width;
            float y = yMin = Random.value * height;
            bee.transform.position = new Vector2(x, y);
        }
        }
    }
        // Update is called once per frame
        void Update () {


        
       
    }
}
