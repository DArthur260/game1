﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
    public float destroyRadius = 1.0f;
    private BeeSpawner beeSpawner;
    public float maxSpeed = 5.0f;
	// Use this for initialization
	void Start () {
        beeSpawner = FindObjectOfType<BeeSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            beeSpawner.DestroyBees(transform.position, destroyRadius);

        }


        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");

        Vector2 velocity = direction * maxSpeed;

        transform.Translate(velocity * Time.deltaTime);
	}
}
